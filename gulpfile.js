var gulp 		= require('gulp');
var jshint 		= require('gulp-jshint');
var changed 	= require('gulp-changed');
var imagemin 	= require('gulp-imagemin');
var minifyhtml  = require('gulp-minify-html');
var concat 		= require('gulp-concat');
var stripDebug	= require('gulp-strip-debug');
var uglify		= require('gulp-uglify');
var autoprefix	= require('gulp-autoprefixer');
var minifyCSS	= require('gulp-minify-css');
var sass 		= require('gulp-sass');
var browserSync = require('browser-sync');
var cache 		= require('gulp-cache');
var livereload  = require('gulp-livereload');
var notify 		= require("gulp-notify");


gulp.task('clean', function() {
  return gulp.src(['./build/css', './build/js', './build/img'], {read: false})
    .pipe(clean());
});

// JS hint task
gulp.task('jshint', function() {
	// body...
	gulp.src('./src/js/*.js')
	.pipe(jshint())
	.pipe(jshint.reporter('default'));
});

// minify new images
gulp.task('imagemin',function () {
	// body...
	var imgSrc = './src/img/**/*',
		imgDst = './build/img';

		gulp.src(imgSrc)
			.pipe(changed(imgDst))
			.pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
			.pipe(gulp.dest(imgDst))
			.pipe(notify({ message: 'Images task complete'}));
});

//minify html
gulp.task('html',function(){
	var htmlSrc = './src/*.html',
		htmlDst = './build';

	gulp.src(htmlSrc)
		.pipe(changed(htmlDst))
		.pipe(minifyhtml())
		.pipe(gulp.dest(htmlDst));
});

//jS concat, strip debugging and minify

gulp.task('main-js',function(){

	gulp.src(['./src/js/main.js'])
		.pipe(concat('main.js'))
		.pipe(stripDebug())
		.pipe(uglify())
		.pipe(gulp.dest('./build/js/'));
});

gulp.task('plugin-js',function(){

	gulp.src(['./src/js/vendor/*.js'])
		.pipe(concat('plugins.js'))
		.pipe(stripDebug())
		.pipe(uglify())
		.pipe(gulp.dest('./build/js/vendor/'));
});


//CSS concat, auto-prefix and minify
gulp.task('css',function(){
	gulp.src(['./src/css/soon.css'])
		.pipe(concat('soon.css'))
		.pipe(autoprefix('last 2 versions'))
		.pipe(minifyCSS())
		.pipe(gulp.dest('./build/css/'));
});


//SCSS 
gulp.task('sass',function(){
	gulp.src('./src/scss/soon.scss')
		.pipe(sass())
		.pipe(gulp.dest('./src/css/'));
});


//Browser sync
gulp.task('browser-sync',function(){
	browserSync({		
		proxy: "http://macmini.local/jam/html/build/",
		host: "127.0.0.1"		
	});

});



//default gulp task
gulp.task('default', ['imagemin','main-js','css','sass','plugin-js'], function(){
	
	// watch for js changes
	gulp.watch('./src/js/main.js',function(){
		gulp.run('jshint','main-js');
	});

	// watch for js changes
	gulp.watch('./src/js/vendor/*.js',function(){
		gulp.run('jshint','plugin-js');
	});
	
	//watch for css changes
	gulp.watch('./src/css/*.css', function(){
		gulp.run('css');
	});

	//watch for css changes
	gulp.watch('./src/img/**/*', function(){
		gulp.run('imagemin');
	});
	
	//watch for scss changes
	gulp.watch('./src/scss/*.scss', function(){
		gulp.run('sass');
	});

});
